package fizzbuzz;

public class App 
{
    public static void main( final String[] args) {
        final App app = new App();

        for (final String element : app.count()) {
            System.out.println(element);
        }
    }

    public String[] count() {

        final String[] countArray = new String[100];

        int count=1;
        for(int i=0; i<countArray.length;++i){

            if(count%3==0 && count%5==0){
                countArray[i]="FizzBuzz";
            }
            else if(count%3==0){
                countArray[i]="Fizz";
            }
            else if(count%5==0){
                countArray[i]="Buzz";
            }
            else {
                countArray[i]=count+"";
            }
            ++count;
        }
        
        return countArray;
	}
}
