package fizzbuzz;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    
    @Test
    public void countLengthTest(){
        
        final App app = new App();
        final String[] actual = app.count();

        assertTrue(actual.length == 100);
    }

    @Test
    public void fizzTest() {

        final App app = new App();
        final String[] actual = app.count();

        assertTrue(actual[2] == "Fizz");
        assertTrue(actual[5] == "Fizz");
        assertTrue(actual[98] == "Fizz");
    }

    @Test
    public void buzzTest() {

        final App app = new App();
        final String[] actual = app.count();

        assertTrue(actual[4] == "Buzz");
        assertTrue(actual[9] == "Buzz");
        assertTrue(actual[99] == "Buzz");
    }

    @Test
    public void fizzBuzzTest() {

        final App app = new App();
        final String[] actual = app.count();

        assertTrue(actual[14]=="FizzBuzz");
        assertTrue(actual[29]=="FizzBuzz");
        assertTrue(actual[89]=="FizzBuzz");
    }
}
